require [
  "app/controllers/WikiController"
  "utils/ember/controllers/CouchDBDocumentController"
  "utils/ember/controllers/CouchDBAttachmentController"
  "app/views/SidebarView"
  "app/views/MaincontentView"
  "app/App"
], (WikiController, getDBController, getAttachmentController, SidebarView, MaincontentView, App) ->

  $.couch.urlPrefix = App.get 'DB_PREFIX'

  dbController = getDBController App.get 'DB_NAME'
  getAssetController =
    getAttachmentController.partial App.get 'DB_PATH'

  wikiController = WikiController.create
    dbController: dbController
    getAssetController: getAssetController
  App.wikiController = wikiController

  sidebarView = SidebarView.create
    controller: wikiController
  sidebarView.appendTo '#sidebar'

  maincontentView = MaincontentView.create
    controller: wikiController
  maincontentView.appendTo '#maincontent'

  wikiController.getMenu()
