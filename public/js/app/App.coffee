# Definition of Applicationwide Constants
DB_NAME = 'couchwiki'
DB_PREFIX = '/db'

@App ?= Ember.Application.create
  DB_PREFIX: (-> DB_PREFIX).property()
  DB_NAME: (-> DB_NAME).property()
  DB_PATH: (-> @get('DB_PREFIX') + '/' + @get('DB_NAME')).property()

define @App
