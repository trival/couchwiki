define [
  'app/models/Document'
  'app/models/DocumentAsset'
  "utils/ember/models/serializableHelpers"
], (Document, DocumentAsset, serialize) ->

  WikiController = Em.ObjectController.extend

    content: null
    menu: []

    dbController: null
    getAssetController: -> null


    assetController: ->
      @getAssetController @get 'content'


    initDoc: (initialData) ->
      doc = Document.create()
      if initialData
        doc.setProperties initialData
      else
        doc.set 'local_edit', true
      @set 'content', doc
      @initAssets() if doc.get '_id'


    initAssets: ->
      assets = @get('content').dataArrayToEmberObjArray DocumentAsset, @get 'assets'
      db = @assetController()
      for asset in assets
        asset.set 'local_url', @assetController().getAssetPath asset
      @set 'local_assets', assets


    loadDoc: (id) ->
      load = =>
        @get('dbController').db.openDoc id,
          success: (doc) => @initDoc doc

      if @get 'local_hasChanges'
        saveDoc load
      else
        load()


    deleteDoc: ->
      doc = @get 'content'
      success = =>
        @removeMenueItem doc
        @set 'content', null
        doc?.destroy()

      if doc?.get '_rev'
        @get('dbController').delete doc, success
      else
        success()


    saveDoc: (callback) ->
      db = @get 'dbController'
      data = @get('content').toJsonData()
      @set 'lastChange', new Date().getTime()
      @updateAssetsData() if @get '_id'

      success = (data) =>
        @set '_id', data.id
        @set '_rev', data.rev
        @set 'local_hasChanges', false
        @set 'local_edit', false
        @syncAssets()
        @getMenu()
        callback() if callback

      error = ->
        @setProperties oldValues

      db.save @get('content').toJsonData(), success, error


    removeMenueItem: (doc) ->
      menu = @get 'menu'
      item = menu.find (item) -> item.get('id') == doc.get '_id'
      menu.removeObject item if item


    updateAssetsData: ->
      assets = @get("local_assets").filter (asset) ->
        asset.get('local_delete') isnt true
      @set 'assets', @get('content').emberObjArrToJsonDataArr assets


    syncAssets: ->
      success = =>
        @initAssets()
      @assetController().syncAll @get('local_assets'), success


    getMenu: ->
      @get('dbController').db.view 'views/menu',
        success: (results) =>
          @set 'menu', []
          for result in results.rows
            @get('menu').pushObject Ember.Object.create result.value


    selectAssetFile: ->
      input = $("#file-selector")
      input.unbind 'change'
      input.change =>
        console.debug input[0].files
        file = input[0].files[0]
        @updateAsset(file) if file

      input.click()


    updateAsset: (file) ->
      unless file and file.name and file.type? and file.size
        throw TypeError "no correct file was given in parameter"

      old = @get("local_assets").filterProperty 'name', file.name
      @get("local_assets").removeObject asset for asset in old

      asset = DocumentAsset.create
        name: file.name
        size: file.size
        local_file: file
        local_upload: true
        local_url: URL.createObjectURL(file)
      asset.set 'date', new Date().getTime()

      asset.set 'local_new', true unless old.length > 0 and not old[0].get 'local_new'

      @get("local_assets").pushObject asset
      @set 'local_hasChanges', true

