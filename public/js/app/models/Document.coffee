define [
  "utils/ember/models/CouchDBDocument"
  "libs/code/markdown"
], (CouchDBDocument) ->

  _type = "page"

  Document = CouchDBDocument.extend

    type: (-> _type).property()

    title: ""
    category: ""
    lastChange: new Date().getTime()
    markdown: ""

    assets: []

    local_assets: []
    local_edit: false
    local_hasChanges: false

    local_timeString: (->
      new Date(@get 'lastChange').toUTCString()
    ).property 'lastChange'

    local_contentHTML: (->
      markdown.toHTML @get 'markdown'
    ).property 'markdown'

    changesObserver: (->
      @set 'hasChanges', true
    ).observes '_id', 'title', 'category', 'markdown'

