define [
  "utils/ember/models/CouchDBAttachment"
], (CouchDBAttachment)->

  DocumentAsset = CouchDBAttachment.extend
    size: ""
    date: 0

    local_url: null
    local_new: false

    local_statusClass: (->
      if @get 'local_delete'
        "icon-trash"
      else if @get('local_upload') and @get('local_new')
        "icon-upload"
      else if @get('local_upload') and not @get('local_new')
        "icon-refresh"
      else
        "icon-ok"
    ).property 'local_delete', 'local_new', 'local_upload'

    local_date: (->
      date = new Date @get 'date'
      "#{date.getFullYear()} - #{date.getMonth() + 1} - #{date.getDate()}"
    ).property 'date'

