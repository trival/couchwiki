define [
  "text!./templates/assetview.hbs"
], (template)->

  AssetView = Ember.View.extend
    template: Ember.Handlebars.compile template
    tagName: 'tr'
    docBinding: 'controller.content'

    delete: ->
      @set 'asset.local_delete', true
      @set 'doc.local_hasChanges', true

    undoDelete: ->
      @set 'asset.local_delete', false

