define [
  "text!./templates/maincontent.hbs"
  "./AssetView"
], (maincontentTpl, AssetView) ->

  MaincontentView = Ember.View.extend
    template: Ember.Handlebars.compile maincontentTpl
    docBinding: 'controller.content'
    assetView: AssetView.extend()

    setView: ->
      @set 'doc.local_edit', false

    setEdit: ->
      @set 'doc.local_edit', true

    saveDoc: ->
      @get('controller').saveDoc()

    deleteDoc: ->
      @get('controller').deleteDoc()

    selectAsset: ->
      @get('controller').selectAssetFile()

    toggleAssets: ->
      $('#assets-container').toggleClass 'hidden'

