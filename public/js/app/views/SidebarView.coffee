define [
  "text!./templates/sidebar.hbs"
], (sidebarTpl) ->

  SidebarView = Ember.View.extend
    template: Ember.Handlebars.compile sidebarTpl

    newDoc: ->
      @get('controller').initDoc()

    menuItem: Ember.View.extend
      tagName: 'li'
      classNameBindings: ['active']
      docBinding: 'controller.content'

      active: (->
        @get('doc._id') == @get 'item.id'
      ).property 'item', 'doc'

      click: (event) ->
        @get('controller').loadDoc @get 'item.id'

