define [
  'utils/ember/models/CouchDBAttachment'
  'utils/ember/models/CouchDBDocument'
  'utils/ember/models/dataValidators'
  'utils/validationHelpers'
  'utils/ajaxHelpers'
  'utils/functionalHelpers'
], (Attachment, Document, validData, validate, ajax) ->

  (dbPath, doc) ->
    unless validate.isNotEmptyString dbPath
      throw Error "dbPath is not valid"

    unless validData.isCouchDBDocument(doc) and validate.isNotEmptyString(doc.get '_id')
      throw Error "Document has to be set properly"

    controller =
      getAssetPath: (asset) ->
        unless validData.isCouchDBAttachment(asset) and validate.isNotEmptyString asset.get 'name'
          throw TypeError "argument is not a valid CouchDBAttachment"

        path = dbPath + "/" + doc.get '_id'
        path += "/" + asset.get 'path' if validate.isNotEmptyString asset.get 'path'
        path += "/" + asset.get 'name'

      save: (asset, success, error) ->
        if (asset.get?('local_upload') is true)
          path = @getAssetPath(asset) + "?rev=" + doc.get '_rev'
          successHandler = (data) =>
            unless data?.ok
              error?()
            else
              doc.set '_rev', data.rev
              success?()

          ajax.sendfile(asset.get('local_file'), path, successHandler, error)

      delete: (asset, success, error) ->
        if (asset.get?('local_delete') is true)
          path = @getAssetPath(asset) + "?rev=" + doc.get '_rev'
          successHandler = (data) =>
            unless data?.ok
              error?()
            else
              doc.set '_rev', data.rev
              success?()
          ajax.deleteAsset(path, successHandler, error)

      syncAll: (assetArray, success, error) ->
        unless $.isArray assetArray
          throw TypeError "saveAll: argument is not an array"

        def = $.Deferred()
        def.resolve()

        for asset in assetArray when asset.get?('local_upload') is true
          do (asset) =>
            console.debug 'saving ' + asset.get 'name'
            tempDef = $.Deferred()
            $.when(def).then => @save asset,
              (-> tempDef.resolve(); console.debug asset.get('name') + " saved"),
              (-> def.reject(); tempDef.reject(); error?(); console.debug asset.get('name') + " not saved")
            def = tempDef

        for asset in assetArray when asset.get?('local_delete') is true
          do (asset) =>
            console.debug 'deleting ' + asset.get 'name'
            tempDef = $.Deferred()
            $.when(def).then => @delete asset,
              (-> tempDef.resolve(); console.debug asset.get('name') + " deleted"),
              (-> def.reject(); tempDef.reject(); error?(); console.debug asset.get('name') + " not deleted")
            def = tempDef

        def.done ->
          $.get dbPath + "/" + doc.get('_id'), ((data) ->
            if doc.get 'content'
              doc.set 'content._attachments', data._attachments
            else
              doc.set '_attachments', data._attachments
            success?()
          ), 'json'

