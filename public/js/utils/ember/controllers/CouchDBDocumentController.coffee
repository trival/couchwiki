define [
  "utils/ember/models/CouchDBDocument"
  "utils/ember/models/dataValidators"
  "utils/validationHelpers"
  ], (Document, data, validate) ->

    (dbName) ->
      unless validate.isNotEmptyString dbName
        throw Error "the argument '#{dbName}' is invalid"

      _db = $.couch.db(dbName)

      _executeDBCallbackOperation = (funcName, obj, success, error) ->
        options = {}
        options.success = success if success
        options.error = error if error

        if options.success or options.error
          _db[funcName] obj, options
        else
          _db[funcName] obj
      
      controller =
        save: (obj, success, error) ->
          if obj instanceof Ember.Object
            throw Error "Ember.Objects must be serialized before saving"
          delete obj._rev unless obj._rev
          delete obj._id unless obj._id
          _executeDBCallbackOperation("saveDoc", obj, success, error)

        delete: (doc, success, error) ->
          throw Error "doc is not an CouchDBDocument" unless data.isCouchDBDocument doc
          obj = doc.getJsonId()
          _executeDBCallbackOperation("removeDoc", obj, success, error)

        db: _db

