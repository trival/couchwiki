define ->

  validators =
    isCouchDBDocument: (doc) ->
      if doc instanceof Ember.Object
        doc.get('_id') isnt undefined and doc.get('_rev') isnt undefined
      else
        doc._id isnt undefined and doc._rev isnt undefined

    isCouchDBAttachment: (attachment) ->
      attachment.name isnt undefined and
        attachment.path isnt undefined and
        attachment.local_file isnt undefined and
        attachment.local_upload isnt undefined and
        attachment.local_delete isnt undefined

