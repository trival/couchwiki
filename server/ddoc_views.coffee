module.exports =

  _id: "_design/views"

  views:

    menu:
      map: (doc) ->
        if doc.type is 'page'
          emit [doc.category, doc.title],
            id: doc._id
            title: doc.title
            category: doc.category
